package com.pushparaj.assignment.countries.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.pushparaj.assignment.countries.common.ResponseStatus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class CountriesRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: CountriesRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = FakeCountriesRepositoryImpl()
    }

    @Test
    fun getAllCountries() {
        runBlocking {
            repository.getAllCountries().collect {
                assertTrue(it is ResponseStatus.SUCCESS)
            }
        }
    }
}
