package com.pushparaj.assignment.countries.data.repository

import com.pushparaj.assignment.countries.common.NullResponseException
import com.pushparaj.assignment.countries.common.FailureResponseException
import com.pushparaj.assignment.countries.common.ResponseStatus
import com.pushparaj.assignment.countries.data.network.APIService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface CountriesRepository {
    fun getAllCountries(): Flow<ResponseStatus>
}

class CountriesRepositoryImpl @Inject constructor(
    private val interfaceService: APIService
) : CountriesRepository {

    override fun getAllCountries(): Flow<ResponseStatus> =
        flow {
            emit(ResponseStatus.LOADING())

            try {
                val response = interfaceService.getCountries()
                if (response.isSuccessful) {
                    response.body()?.let {
                        emit(ResponseStatus.SUCCESS(it))
                    } ?: throw NullResponseException()
                } else {
                    throw FailureResponseException()
                }
            } catch (e: Exception) {
                emit(ResponseStatus.ERROR(e))
            }
        }
}
class FakeCountriesRepositoryImpl @Inject constructor(
) : CountriesRepository {

    override fun getAllCountries(): Flow<ResponseStatus> =
        flow {
            emit(ResponseStatus.SUCCESS(listOf()))
        }
}



