package com.pushparaj.assignment.countries.common

class NullResponseException(message: String = "Response is null") : Exception(message)
class FailureResponseException(message: String = "Error: failure in the response") : Exception(message)