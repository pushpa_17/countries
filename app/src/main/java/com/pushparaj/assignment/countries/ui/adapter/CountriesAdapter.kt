package com.pushparaj.assignment.countries.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pushparaj.assignment.countries.data.model.Country
import com.pushparaj.assignment.countries.databinding.CountryItemBinding

class CountryAdapter(
    private val countriesData: MutableList<Country> = mutableListOf()
) : RecyclerView.Adapter<CountryViewHolder>() {

    fun setNewCountries(newDataSet: List<Country>) {
        countriesData.clear()
        countriesData.addAll(newDataSet)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder =
        CountryViewHolder(
            CountryItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) =
        holder.bind(countriesData[position])

    override fun getItemCount(): Int = countriesData.size
}

class CountryViewHolder(
    private val binding: CountryItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(countries: Country) {
        binding.countryCode.text = countries.code
        binding.countryName.text = String.format(countries.name + ",")
        binding.countryRegion.text = countries.region
        binding.countryCapital.text = countries.capital
    }

}