package com.pushparaj.assignment.countries.common

import com.pushparaj.assignment.countries.data.model.Country


sealed interface ResponseStatus {
    class LOADING(val isLoading: Boolean = true) : ResponseStatus
    class SUCCESS(val countries: List<Country>) : ResponseStatus
    class ERROR(val error: Throwable) : ResponseStatus
}